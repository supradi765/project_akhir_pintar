<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Listing extends Model
{
    use HasFactory;

    protected $table = "listings";

    public $timestamps = true;

    protected $fillable = [
        'title',
        'company',
        'location',
        'logo'
    ];

    // protected $guarded = [];

    // public function getRouteKeyName()
    // {
    //     return 'slug';
    // }

    public function clicks()
    {
        return $this->hasMany(Click::class);
    }

    // public function user()
    // {
    //     return $this->belongsTo(User::class);
    // }

    // public function tags()
    // {
    //     return $this->belongsToMany(Tag::class);
    // }
}
