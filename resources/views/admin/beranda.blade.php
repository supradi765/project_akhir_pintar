@extends('templates.default_admin')

@section('content')
    <p class="text-danger">
        @if (session('error'))
            {{ session('error') }}
        @endif
    </p>
    <div class="page-header d-print-none">
        <div class="container-xl">
            <div class="row g-2 align-items-center">
                <div class="col">
                    <!-- Page pre-title -->
                    <h2 class="page-pretitle">
                        Halo {{ Auth::user()->name }}, Selamat Datang di Beranda Admin
                    </h2>
                    <h2 class="page-title">
                        Tersedia {{ $listings->count() }} Website
                    </h2>
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="page-body">
        <div class="container-xl ">
            <div class="row row-deck row-cards ">
                @foreach ($listings as $key => $listing)
                    <div class="col-sm-4 col-lg-4 gap-3">
                        <div class="card card-sm">
                            <a href="#" class="d-block">
                                <img src="/storage/{{ $listing->logo }}" class="card-img-top"></a>
                            <div class="card-body">
                                <div>
                                    <div>
                                        <div>{{ $listing->title }}</div>
                                        <div><b>Harga= Rp.{{ $listing->location }}</b></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
